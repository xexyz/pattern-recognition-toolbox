//Create Bayes Decision Rule
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

//generates econmic gain
float** genEcomGain(int nc)
{
	float **result;
	result = new float*[nc];
	for (int i = 0; i < nc; ++i)
    	result[i] = new float[nc];
		
	for (int i = 0; i < nc; ++i)	
		for (int j = 0; j < nc; ++j)
				if (i == j)
					result[i][j] = 1;
		
	return result;	
}

float* genBayesDes(float **prob, float **ecomGain, int nd, int nc)
{
	float *result = new float[nc];
	
	for (int i = 0; i < nc; ++i)	
	{
		float max = 0;
		int inx = 0;
		for (int j = 0; j < nd; ++j)
		{
			if ((prob[i][j] * ecomGain[j][j]) > max)
			{
				max = prob[i][j] * ecomGain[j][j];
				inx = j;
			}
		}
		result[i] = inx;
	}
	
	return result;
}