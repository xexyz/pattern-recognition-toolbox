#include <stdlib.h>
#include <stdio.h>
#include "im.h"
#include "bdr.h"
#include "cm.h"
#include "eg.h"

void printMatrix(float **array, int xSize, int ySize)
{
	for (int i = 0; i < ySize; ++i)
	{
		for (int j = 0; j < xSize; ++j)
			printf("%f ", array[i][j]);
		
		printf("\n");
	}
}

void printVector(float *array, int size)
{
	for (int i = 0; i < size; ++i)
		printf("%f ", array[i]);
		
	printf("\n");
}

//main entry
int main(int argc, char *argv[]) 
{
	if (argc != 6)
	{
		printf ("Wrong arguments.\n");
		return -1;
	}
	srand(atoi(argv[5]));
	
	int nd = atoi(argv[1]); //# of dimensions
	int nc = atoi(argv[2]); //# of classes
	int ndom = atoi(argv[3]); //# of class dominamt mesurmants
	float fd = atof(argv[4]); //dominant fraction
	
	float **prob = genProbMat (nd, nc, ndom, fd);
	if (!verify(prob, nd, nc, ndom, fd))
		return -1;
	float **ecomGain = genEcomGain(nc);
	float *bayes = genBayesDes(prob, ecomGain, nd, nc);
	float **conf = genConf(prob, bayes, nd, nc);

	printf("Expected Gain = %f \n", expGain(ecomGain, conf, nd, nc));
	return 0;
} //end
