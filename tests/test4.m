function doit4(M)
of = 15;
    tp1= 'ND';
    tp2= 'fd';
    xl = 'Nd';
    yl = 'NC';
for i=0:((300/of)-1)
    name = strcat('Z:\patreg\project\img\5b.',int2str(i+1),'.jpg');
   
  
    
     x2= M((1+(i*of)):(of+(i*of)),3);
    y2 = M((1+(i*of)):(of+(i*of)),4);
    z2 = M((1+(i*of)):(of+(i*of)),5);
    e = std(z2);
    scatter3(x2,y2,z2,'g','DisplayName','untitled(276:300,2:4)');hold on;
   
    tt1=[floor(min(min(x2))):0.005:max(max(x2))];
    tt2=[floor(min(min(y2))):0.1:max(max(y2))];

    [xg,yg]=meshgrid(tt1,tt2);

    zg=griddata(x2, y2, z2, xg,yg,'linear');

    mesh(xg,yg,zg),
    figure(gcf)
    title(strcat(tp1, ' = ', num2str(M(1+(i*of),1)), '   ', tp2, ' = ', num2str(M(1+(i*of),2))));
    axis([0.8 1 2 10])
    xlabel(xl)
    ylabel(yl)
    zlabel('Expected Gain')
    hold off;
    
    saveas(figure(gcf),name,'jpg');
end