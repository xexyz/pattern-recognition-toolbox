function doit(M)
for i=0:11
    name = strcat('Z:\patreg\project\img\4b.',int2str(i+1),'.jpg');
    x = M((1+(i*25)):(5+(i*25)),2);
    y = [M((1+(i*25)),3);M((6+(i*25)),3);M((11+(i*25)),3);M((16+(i*25)),3);M((21+(i*25)),3)];
    z = horzcat(M((1+(i*25)):(5+(i*25)),4),M((6+(i*25)):(10+(i*25)),4),M((11+(i*25)):(15+(i*25)),4),...
        M((16+(i*25)):(20+(i*25)),4),M((21+(i*25)):(25+(i*25)),4));
    %surf(x,y,z,'DisplayName','untitled(276:300,2:4)');
    
     x2= M((1+(i*25)):(25+(i*25)),2);
    y2 = M((1+(i*25)):(25+(i*25)),3);
    z2 = M((1+(i*25)):(25+(i*25)),4);
    e = std(z2);
    scatter3(x2,y2,z2,'g','DisplayName','untitled(276:300,2:4)');hold on;
    for j=1:length(x2)
        xV = [x2(j); x2(j)];
        yV = [y2(j); y2(j)];
        zMin = z2(j) + e;
        zMax = z2(j) - e;

        zV = [zMin, zMax];
        % draw vertical error bar
        h=plot3(xV, yV, zV, '-k');
        set(h, 'LineWidth', 2);
    end
    tt1=[floor(min(min(x2))):0.005:max(max(x2))];
    tt2=[floor(min(min(y2))):0.001:max(max(y2))];

    [xg,yg]=meshgrid(tt1,tt2);

    zg=griddata(x2, y2, z2, xg,yg,'linear');

    mesh(xg,yg,zg),
    figure(gcf)
    title(strcat('ND = ', int2str(M(1+(i*25),1)), ' NC = ', int2str(M(1+(i*25),5))));
    axis([0.8 1 0.8 1])
    xlabel('fd')
    ylabel('Nd')
    zlabel('Expected Gain')
    hold off;
    
    saveas(figure(gcf),name,'jpg');
end