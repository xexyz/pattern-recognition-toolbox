#include <stdlib.h>
#include <stdio.h>
#include <math.h>

//generate the matrix
float** sampledMatrix(float **matrix, int samples, int nd, int nc)
{	
	float *samLine;
	samLine = new float[nd * nc + 1];
	samLine[0] = 0;
	for (int i = 0; i < nc; ++i)
	{
		for (int j = 0; j < nd; ++j)
		{
			//if ( i == 0 && j == 0)
				
			//else
				samLine[(i * nd) + j+1] = samLine[((i * nd) + j)] + matrix[j][i];	
		}
	}
	samLine[nd * nc] = samLine[nd * nc-1] + matrix[nd-1][nc-1];

	int *counter;
	counter = new int[nd * nc];
	for (int i = 0; i < nd * nc; ++i)
		counter[i] = 0;
		
	for (int i = 0; i < samples; i++)
	{
		float value = ((rand() % samples) / (float)samples);
		for(int j = 0; j < (nd * nc); j++)
		{
			if (value <= samLine[j+1] && value >= samLine[j])
				counter[j]++;	
		}
	}

	float **result;
	result = new float*[nd];
	for (int i = 0; i < nd; ++i)
    	result[i] = new float[nc];	
		
	for(int i = 0; i < (nd * nc); i++)
		result[i % nd][i / nd] = counter[i] / (float)samples;
	
		
	return result;
}
