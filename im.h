#include <stdlib.h>
#include <stdio.h>
#include <math.h>

float* randArray(float sum, int n)
{
	float *result;
	result = new float[n];
	int csum = 0;
	for (int i = 0; i < n; ++i)
	{
		int tmp = rand() % 100;
		result[i] = tmp;
		csum += tmp;
	}

	for (int i = 0; i < n; ++i)
		result[i] = sum * ( result[i] / csum );

	return result;
}

//generate the matrix
float** genProbMat(int nd, int nc, int ndom, float fd)
{
	float **result;
	result = new float*[nd];
	for (int i = 0; i < nd; ++i)
	result[i] = new float[nc];

	float *pd = randArray(1, nd);

	int doms = ndom;
	for (int i = 0; i < nd; ++i)
	{
		if (doms > 0)
		{
				int inx = rand() % nc;
				result[i][inx] = fd * pd[i] * (.9 + (.1 * ((rand() % 100) / 100)));

				float newPd = pd[i] - result[i][inx];
				int csum = 0;
				for(int j = 0; j < nc; ++j)
				{
					if (inx == j)
						continue;
					int tmp = rand() % 100;
					result[i][j] = tmp;
					csum += tmp;
				}
				for(int j = 0; j < nc; ++j)
				{
					if (inx == j)
						continue;
					result[i][j] = newPd * ( result[i][j] / csum );
				}

			doms--;
		}
		else
			result[i] = randArray(pd[i], nc);
	}
	return result;
}

//verify the matrix
bool verify(float **array, int nd, int nc, int ndom, float fd)
{
	//check if sum is correct
	float sum = 0;
	for (int i = 0; i < nd; ++i)
	{
		for (int j = 0; j < nc; ++j)
		{
			sum += array[i][j];
		}
	}
	if (sum != 1)
		return false;

	//check dominants
	int doms = 0;
	for (int i = 0; i < nd; ++i)
	{
		float max = 0;
		sum = 0;
		for (int j = 0; j < nc; ++j)
		{
			if (array[i][j] > max)
				max = array[i][j];
			sum += array[i][j];		
		}

		if (floorf(max * 1000) / 1000
			== floorf((sum * fd * .9)* 1000) / 1000)
			doms++;
	}
	if (doms >= ndom)
		return true;

	return false;
}
//end im.h
