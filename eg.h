//Create Expected Gain
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

float expGain(float **ecomGain, float **conf, int nd, int nc)
{
	float sum = 0;
	for (int i = 0; i < nc; ++i)	
		for (int j = 0; j < nc; ++j)
			 sum += ecomGain[i][j] * conf[i][j];
		
	return sum;		
}