# Pattern Recognition Toolbox

The tools are
- `bdr.h` Bayes Decision Rule
- `cm.h` Confusion Matrix Gen
- `eg.h` Expected Gain
- `im.h` Initial Matrix Gen
- `mm.h` Maxmin Decision Rule
- `sam.h` Matrix Sampler

Build and run
