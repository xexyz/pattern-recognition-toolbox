function doit2(M)
for i=0:24
    name = strcat('Z:\patreg\project\img\4.',int2str(i+1),'.jpg');
    y = M(1:4,3);
    x = [M(1,4);M(5,4);M(9,4)];
    z = horzcat(M((1+(i*12)):(4+(i*12)),5),M((5+(i*12)):(8+(i*12)),5),M((9+(i*12)):(12+(i*12)),5));
    %surf(x,y,z,'DisplayName','untitled(276:300,2:4)');hold on;
   
    x2= M((1+(i*12)):(12+(i*12)),4);
    y2 = M((1+(i*12)):(12+(i*12)),3);
    z2 = M((1+(i*12)):(12+(i*12)),5);
    e = std(z2);
    scatter3(x2,y2,z2,'g','DisplayName','untitled(276:300,2:4)');hold on;
    for j=1:length(x2)
        xV = [x2(j); x2(j)];
        yV = [y2(j); y2(j)];
        zMin = z2(j) + e;
        zMax = z2(j) - e;

        zV = [zMin, zMax];
        % draw vertical error bar
        h=plot3(xV, yV, zV, '-k');
        set(h, 'LineWidth', 2);
    end
    tt1=[floor(min(min(x2))):0.25:max(max(x2))];
    tt2=[floor(min(min(y2))):0.5:max(max(y2))];

    [xg,yg]=meshgrid(tt1,tt2);

    zg=griddata(x2, y2, z2, xg,yg,'linear');

    mesh(xg,yg,zg),
    figure(gcf)
    title(strcat('fd = ', num2str(M(1+(i*12),1)), ' Nd = ', num2str(M(1+(i*12),2))));
    xlabel('NC')
    ylabel('ND')
    zlabel('Expected Gain')
    hold off;
    saveas(figure(gcf),name,'jpg');
end