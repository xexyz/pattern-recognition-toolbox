//Create Confusion Matrix
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

float** genConf(float **prob, float *bayes, int nd, int nc)
{
	float **result;
	result = new float*[nc];
	for (int i = 0; i < nc; ++i)
    	result[i] = new float[nc];
		
	for (int i = 0; i < nc; ++i)	
	{		
		for (int j = 0; j < nc; ++j)
		{
			float sum = 0;
			for (int k = 0; k < nd; ++k)
			{			
				if (j == bayes[k])
					sum += prob[k][j];
			}
			result[i][j] = sum;
		}
	}
		
	return result;	
}
