#include <stdlib.h>
#include <stdio.h>
#include <math.h>

//generate the maximin decision rule
float* maximin(float **prob, float **ecomGain, int nd, int nc)
{	
	float *result = new float[nd];
	int total = pow(nd, nc);
	float **combs = new float*[total];
	for (int i = 0; i < total; ++i)
    	combs[i] = new float[nd];
		
	//worst priors
	float *worstPrior;
	worstPrior = new float[nd];
	
	for (int i = 0; i < nd; ++i)
    	worstPrior[i] = i;
		
		
	//get decision rules	
	int *start = new int[nc];
	for (int i = 0; i < nc; i++)
		start[i] = i;
		
	int perm=1;
	for (int i=1;i<=nc;perm*=i++);
	{
		for (int j=0;j<perm;j++)
		{
			int *avail=start;
			for (int k=nc,div=perm;k>0; k--) 
			{
				div/=k;
				int index = (j/div)%k;
				//printf("%d", avail[index] );
				avail[index]=avail[k-1];				
			}
			//printf("\n");
		}
	}
	
	//solve	
	int inx = 0;
	int csum = 0;
	for(int j = 0; j < nc; ++j)
	{
		if (inx == j)
			continue;
		int tmp = rand() % 100;
		result[j] = tmp;
		csum += tmp;
	}
	for(int j = 0; j < nc; ++j)
	{
		if (inx == j)
			continue;
		result[j] = ( result[j] / csum );

	}
	return result;
}

//class conditional probablity
float** conProb(float **prob, int nd, int nc)
{	
	float **result;
	result = new float*[nd];
	for (int i = 0; i < nd; ++i)
    	result[i] = new float[nc];	

	for (int i = 0; i < nc; ++i)
	{
		float sum = 0;
		for (int j = 0; j < nd; ++j)
		{
			sum += prob[j][i];
			
			}
			//printf("%f \n", sum);
		for (int j = 0; j < nd; ++j)
			result[j][i] = prob[j][i] / sum;
	}
	return result;
}
