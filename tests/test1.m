function doit(M)
for i=0:11
    name = strcat('Z:\patreg\project\img\5c.',int2str(i+1),'.jpg');
    x = M((1+(i*25)):(5+(i*25)),2);
    y = [M((1+(i*25)),3);M((6+(i*25)),3);M((11+(i*25)),3);M((16+(i*25)),3);M((21+(i*25)),3)];
    z = horzcat(M((1+(i*25)):(5+(i*25)),4),M((6+(i*25)):(10+(i*25)),4),M((11+(i*25)):(15+(i*25)),4),...
        M((16+(i*25)):(20+(i*25)),4),M((21+(i*25)):(25+(i*25)),4));
    surf(x,y,z,'DisplayName','untitled(276:300,2:4)');hold on;
    
    
    figure(gcf)
    title(strcat('ND = ', int2str(M(1+(i*25),1)), ',NC = ', int2str(M(1+(i*25),5)), ',x10 Samples'));
    axis([0.8 1 0.8 1])
    xlabel('fd')
    ylabel('Nd')
    zlabel('Expected Gain')
    hold off;
    saveas(figure(gcf),name,'jpg');
end